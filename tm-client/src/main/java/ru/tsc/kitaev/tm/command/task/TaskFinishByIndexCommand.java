package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by index...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskEndpoint().finishTaskByIndex(session, index);
    }

}
