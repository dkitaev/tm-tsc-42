package ru.tsc.kitaev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractOwnerModel extends AbstractModel {

    @NotNull
    @ManyToOne
    protected User user;

}
