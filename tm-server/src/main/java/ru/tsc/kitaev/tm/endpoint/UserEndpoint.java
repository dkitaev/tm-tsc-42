package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint implements ru.tsc.kitaev.tm.api.endpoint.IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

    @Override
    @WebMethod
    public void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
