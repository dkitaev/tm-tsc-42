package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;

import java.util.List;

public interface ISessionService {

    void clear();

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findAll(@Nullable final String sort);

    @Nullable
    SessionDTO findById(@NotNull final String id);

    @NotNull
    SessionDTO findByIndex(@NotNull final Integer index);

    void removeById(@NotNull final String id);

    void removeByIndex(@NotNull final Integer index);

    boolean existsById(@NotNull final String id);

    boolean existsByIndex(final int index);

    int getSize();

    @NotNull
    SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable
    UserDTO checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@Nullable SessionDTO session, Role role);

    void validate(@Nullable SessionDTO session);

    @NotNull
    SessionDTO sign(@NotNull SessionDTO session);

    void close(@Nullable SessionDTO session);

}
