package ru.tsc.kitaev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public final class User extends AbstractModel {

    @NotNull
    @Column
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

}
